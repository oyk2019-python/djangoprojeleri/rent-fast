from django.db import models
from django.contrib.auth.models import User, AbstractUser

class Users(AbstractUser):
    phone=models.CharField(max_length=11, null=True,blank=True)
    birthday = models.DateTimeField(null=True, blank=True)
    location = models.CharField(null=True, max_length=20, blank=True)
    image = models.ImageField(null=True, blank=True)
    followed = models.ManyToManyField('self', related_name='followers',null=True,blank=True)















