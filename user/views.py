from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .models import Users
from .forms import NewUserCreation,ProfileUpdateForm
from django.contrib.auth import get_user_model
from django.views.generic import DetailView, CreateView, UpdateView

user=get_user_model()
class CreateViewCBV(CreateView):
    model = user
    form_class = NewUserCreation
    success_url="/admin"
    template_name = 'users/register.html'

class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = user
    form_class = ProfileUpdateForm
    template_name = "users/profile_update.html"

    def get_success_url(self):
        return reverse_lazy("me")

    def get_object(self, queryset=None):
        return self.request.user

    class Meta(user.Meta):
        model = user
        fields = [
            'first_name', 'last_name', 'email', 'telephone', 'birthday', 'location', 'image',
        ]


class UserDetailView(LoginRequiredMixin, DetailView):
    model = user
    template_name = 'users/users_detail.html'
    is_me = False

    def get_object(self, queryset=None):
        if self.is_me:
            return self.request.user
        else:

            return super().get_object(queryset)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["is_me"] = self.is_me
        return context
