from django.contrib import admin
from django.urls import path,include
from django.urls import path,include
from django.views.generic import TemplateView

from .views import CreateViewCBV,ProfileUpdateView,UserDetailView

urlpatterns = [
    path('register',CreateViewCBV.as_view(),name='register'),
    path('profile/', ProfileUpdateView.as_view(), name='update_profile'),
    path('detail/me/', UserDetailView.as_view(is_me=True), name='me'),
    path('detail/<int:pk>/', UserDetailView.as_view(), name='user_detail'),
    path('accounts/', include('django.contrib.auth.urls')),


]
