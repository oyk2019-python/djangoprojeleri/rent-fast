from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Users
from django.contrib.auth import get_user_model

@admin.register(Users)
class UserTable(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        ('Custom Fields', {'fields': (
            'phone', 'birthday', 'location', 'image',)}),
    )



# Register your models here.
