from django import forms
from .models import Users
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model

user=get_user_model()
class RealDateInput(forms.DateInput):
    input_type = "date"

class NewUserCreation(UserCreationForm,forms.ModelForm):
    email=forms.EmailField(help_text='Email adresini girinz!')
    class Meta(UserCreationForm.Meta):
        model = user
        fields = UserCreationForm.Meta.fields+("email","first_name","last_name","phone","birthday","location","image")
        widgets={"birthday":RealDateInput}



class ProfileUpdateForm(forms.ModelForm):
    class Meta(user.Meta):
        model = user
        fields = [
            "email","first_name","last_name","phone","birthday","location","image",
        ]
        widgets = {"birthday": RealDateInput}