# Generated by Django 2.2.3 on 2019-07-24 18:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0004_productcomment_parent'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ProductComment',
        ),
    ]
