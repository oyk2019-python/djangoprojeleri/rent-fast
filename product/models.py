from django.db import models
from django.conf import settings

class Product(models.Model):
    TYPES=(
        (1,"arac"),
        (2,"giyim"),
        (3,"konut"),
        (3, "diger")
    )
    name=models.CharField(max_length=25)
    owner=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    description=models.CharField(max_length=200)
    pro_type=models.IntegerField(choices=TYPES,default=1,blank=True,null=True)
    location=models.CharField(max_length=20)
    is_active=models.BooleanField(default=True)
    image=models.ImageField(null=True,blank=True)

    def __str__(self):
        return self.name




class Rental(models.Model):
    user_name=models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    pro_name=models.CharField(max_length=25)
    created=models.DateTimeField(auto_now_add=True)

