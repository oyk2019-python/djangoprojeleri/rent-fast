from django.shortcuts import render
from django.shortcuts import reverse, redirect
from django.views.generic import CreateView, ListView, RedirectView, DetailView, UpdateView
from .models import Product,Rental
from django.db.models import Q

class ProductListView(ListView):
    model = Product
    template_name = "rental/product_list.html"
    def get_queryset(self):
        qs = Q(is_active=True)
        if self.request.user.is_authenticated:
            qs |= Q(owner=self.request.user)
        return super().get_queryset().filter(qs)

class ProductDetailView(DetailView):
    model = Product
    template_name = "rental/product_details.html"

class ProductRentalView(DetailView):
    model = Product
    template_name = "rental/kiralama.html"


