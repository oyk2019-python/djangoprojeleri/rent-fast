from django.urls import path

from communication.views import CreateProComment
from .views import ProductListView, ProductDetailView, ProductRentalView

urlpatterns = [
    path("list/", ProductListView.as_view(), name="product_list"),
    path("detail/<int:pk>/", ProductDetailView.as_view(), name="item_detail"),
    path("kiralama/<int:pk>/", ProductRentalView.as_view(), name="kiralama"),


]
