from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.shortcuts import render
from user.models import Users
from django.urls import reverse
from django.utils import timezone

from communication.forms import MessageForm, ProductCommentForm, UserCommentForm, CreateProForm
from communication.models import Messages, ProductComment, UserComment
from django.views.generic import ListView, DetailView, RedirectView, FormView, CreateView


# Create your views here.


class MessagesListView(LoginRequiredMixin, ListView):
    model = Messages

    def get_queryset(self):
        return Messages.objects.filter(receiver=self.request.user).values("sender").distinct().annotate()


class MessagesDetailView(LoginRequiredMixin, ListView):
    model = Messages
    queryset = Messages.objects.all()
    template_name = "communication/messages_detail.html"

    def get_queryset(self):
        super().get_queryset().filter(
            Q(sender_id=self.kwargs["user_id"], receiver=self.request.user) & Q(seen__isnull=True)
        ).update(seen=timezone.now())
        query =super().get_queryset().filter(
            Q(sender_id=self.kwargs["user_id"], receiver=self.request.user) |
            Q(receiver_id=self.kwargs["user_id"], sender=self.request.user)
        ).order_by("created")
        total = query.count()
        limit = 50
        offset = total - limit if total - limit > 0 else 0
        return query[offset:]

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["other_user"] = Users.objects.get(id=self.kwargs["user_id"])
        context["form"] = MessageForm()
        return context


class MessagesSendView(LoginRequiredMixin, FormView):
    form_class = MessageForm

    def get_success_url(self):
        return reverse("messages_detail", kwargs={"user_id": self.kwargs["user_id"]})

    def form_valid(self, form):
        Messages.objects.create(
            sender=self.request.user,
            receiver_id=self.kwargs["user_id"],
            message=form.cleaned_data["message"],
            file=form.cleaned_data["file"]
        )
        return super().form_valid(form)

class ProCommentListView(LoginRequiredMixin, ListView):
    model = Messages
    template_name = 'communication/procomment_list.html'


class UserCommentListView(LoginRequiredMixin, ListView):
    model = Messages
    template_name = 'communication/comment_list.html'

class CreateProComment(LoginRequiredMixin,CreateView):
    model = ProductComment
    template_name ="communication/product_comment.html"
    form_class =ProductCommentForm
    success_url = "UserCommentForm"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.product = None

    def __set__(self, instance, value):
        product = ProductComment.product
        return self.product

class CreateUserComment(LoginRequiredMixin,CreateView):
    model = UserComment
    template_name ="communication/user_comment.html"
    form_class =UserCommentForm
    success_url = "users/profile.html"


class CreateProduct(CreateView):
    model = ProductComment
    form_class = CreateProForm
    success_url = "user/profile"
    template_name = 'users/add_product.html'
