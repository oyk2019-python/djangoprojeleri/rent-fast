from django.db import models
from django.conf import settings
from product.models import Product




class UserComment(models.Model):
    owner=models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,related_name='Owner')
    parent=models.ForeignKey('self',on_delete=models.SET_NULL,null=True,blank=True)
    user=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,related_name='user')
    comment_text=models.TextField(max_length=250)
    created=models.DateTimeField(auto_now_add=True)

    @property
    def short_comment_user(self):
        if len(self.comment_text) > 50:
            return self.comment_text[:47] + "..."


    def __str__(self):
        return self.short_comment_user



class ProductComment(models.Model):
    product=models.ForeignKey(Product,on_delete=models.CASCADE)
    owner=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.SET_NULL,null=True,blank=True)
    parent=models.ForeignKey('self',on_delete=models.SET_NULL,null=True,blank=True)
    comment=models.TextField(max_length=250)
    created=models.DateTimeField(auto_now_add=True)
    image = models.ImageField(null=True, blank=True)

    @property
    def short_comment_product(self):
        if len(self.comment) > 50:
            return self.comment[:47] + "..."

    def __str__(self):
        return self.short_comment_product


def determine_message_file(instance, filename):
    low = min([instance.sender.id, instance.receiver.id])
    high = max([instance.sender.id, instance.receiver.id])
    return f"messages/{low}_{high}/{filename}"


class Messages(models.Model):
    sender=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.SET_NULL,null=True,related_name='message_from_me')
    receiver=models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.SET_NULL,null=True,related_name='message_to_me')
    message=models.TextField(max_length=140)
    created=models.DateTimeField(auto_now_add=True)
    seen=models.DateTimeField(blank=True,null=True)
    file=models.FileField(blank=True,null=True)

    @property
    def short_message(self):
        if len(self.message) > 20:
            return self.message[:17] + "..."


