from django.urls import path
from .views import MessagesListView, MessagesDetailView, MessagesSendView, CreateProComment, UserCommentListView, \
    ProCommentListView

urlpatterns = [
    path('list/', MessagesListView.as_view(), name='messages_list'),
    path('detail/<int:user_id>/', MessagesDetailView.as_view(), name='messages_detail'),
    path('detail/<int:user_id>/send', MessagesSendView.as_view(), name='messages_send'),
    path("createprocomment/", CreateProComment.as_view(), name="create_procomment"),
    path("procommentadd/", UserCommentListView.as_view(), name="create_usercomment"),
    path("procomment/list/", ProCommentListView.as_view(), name="procomment_list"),

]
