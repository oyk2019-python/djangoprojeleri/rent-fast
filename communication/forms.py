from django import forms
from django.utils.translation import gettext_lazy as _

from communication.models import ProductComment,UserComment


class MessageForm(forms.Form):
    message = forms.CharField(max_length=140, label=_("Message"))
    file = forms.FileField(required=False, label=_("File"))

class ProductCommentForm(forms.ModelForm):
    class Meta:
        model = ProductComment
        fields = ['product','parent','owner','comment','image']

class UserCommentForm(forms.ModelForm):
    class Meta:
        model = UserComment
        fields = ['user','owner','parent','comment_text',]


class CreateProForm(forms.ModelForm):
    class Meta:
        model = ProductComment
        fields = ['product', 'owner', 'parent', 'comment' ]
